val scalatestVersion = "3.2.0"
val mockitoScalaVersion = "1.15.0"
val enumeratumVersion = "1.6.1"

lazy val commonSettings = Seq(
  organization := "fr.valwin",
  version := "1.0.0-SNAPSHOT",
  scalaVersion := "2.13.3",
  scalacOptions -= "-language:experimental.macros",
  scalacOptions += "-language:higherKinds",
  libraryDependencies ++= Seq(
    "org.scalactic" %% "scalactic" % scalatestVersion,
    "org.scalatest" %% "scalatest" % scalatestVersion % Test,
    "org.mockito" %% "mockito-scala" % mockitoScalaVersion % Test,
    "com.beachape" %% "enumeratum" % enumeratumVersion
  ),
  addCompilerPlugin(
    "org.typelevel" %% "kind-projector" % "0.11.0" cross CrossVersion.full
  )
)

lazy val root = (project in file("."))
  .settings(commonSettings)
  .settings(name := "valwin-scala-exercice")
