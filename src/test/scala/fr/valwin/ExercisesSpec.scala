package fr.valwin

import org.scalatest.flatspec.AnyFlatSpec
import org.scalatest.matchers.should.Matchers

class ExercisesSpec extends AnyFlatSpec with Matchers {

  "Exercise 1" should "succeed" in {
    Exercises.res1 should contain theSameElementsAs (List(
      "TOTO",
      "TITI",
      "CACA"
    ))
  }
  "Exercise 2" should "succeed" in {
    Exercises.res2 shouldBe 2020
    Exercises.res3 shouldBe 42
  }
  "Exercise 3" should "succeed" in {
    Exercises.res4 should contain theSameElementsAs (List(
      "Toto",
      "titi",
      "CACA"
    ))
  }
  "Ecercise 4" should "succeed" in {
    Exercises.res5 should contain theSameElementsAs (List(4.00, 2.00))
  }

}
