package fr.valwin.library

import fr.valwin.library.model.Book
import org.scalatest.GivenWhenThen
import org.scalatest.matchers.should.Matchers
import org.scalatest.wordspec.AnyWordSpec

class AccountingSpec extends AnyWordSpec with Matchers with GivenWhenThen {

  "Accounting" should {
    "correctly build a basket from a list of books" in {
      Given("a list of various books")
      val books: List[Book] = List(
        Book.GAME_OF_THRONES_1,
        Book.GAME_OF_THRONES_2,
        Book.HARRY_POTTER_1,
        Book.LORD_OF_THE_RINGS_1,
        Book.LORD_OF_THE_RINGS_2,
        Book.LORD_OF_THE_RINGS_3
      )

      When("building a basket")
      val basket = Accounting.buildBasket(books: _*)

      Then("basket contains the list of various books")
      basket.books should contain theSameElementsAs books
    }

    "correctly compute basket final price" when {
      "best promotion is TwoDifferentBooksFromTheSameCollection (LOTR)" in {
        Given("a list of various books")
        val books: List[Book] = List(
          Book.GAME_OF_THRONES_1,
          Book.GAME_OF_THRONES_2,
          Book.GAME_OF_THRONES_2,
          Book.GAME_OF_THRONES_2,
          Book.LORD_OF_THE_RINGS_1,
          Book.LORD_OF_THE_RINGS_1,
          Book.LORD_OF_THE_RINGS_2
        )

        When("computing final price")
        val finalPrice = Accounting.checkout(Accounting.buildBasket(books: _*))

        Then(
          "final price is 92.86"
        ) // 9.90 + (10.40 * 3) + (19.90 * 2 + 24.90) * 0.80
        finalPrice shouldBe 92.86
      }
      "best promotion is AtLeastThreeDifferentBooksFromTheSameCollection (GOT)" in {
        Given("a list of various books")
        val books: List[Book] = List(
          Book.GAME_OF_THRONES_1,
          Book.GAME_OF_THRONES_1,
          Book.GAME_OF_THRONES_1,
          Book.GAME_OF_THRONES_2,
          Book.GAME_OF_THRONES_2,
          Book.GAME_OF_THRONES_4,
          Book.GAME_OF_THRONES_4,
          Book.GAME_OF_THRONES_4,
          Book.GAME_OF_THRONES_4,
          Book.LORD_OF_THE_RINGS_1,
          Book.LORD_OF_THE_RINGS_1,
          Book.LORD_OF_THE_RINGS_2,
          Book.LORD_OF_THE_RINGS_2,
          Book.LORD_OF_THE_RINGS_2
        )

        When("computing final price")
        val finalPrice = Accounting.checkout(Accounting.buildBasket(books: _*))

        Then(
          "final price is 181.77€"
        ) // ((9.90 * 3) + (10.40 * 2) + (11.40 * 4)) * 0.70 + (19.90 * 2) + (24.90 * 3)
        finalPrice shouldBe 181.77
      }
      "best promotion is OneBookFromTwoDifferentCollection" in {
        Given("a list of various books")
        val books: List[Book] = List(
          Book.GAME_OF_THRONES_1,
          Book.GAME_OF_THRONES_2,
          Book.LORD_OF_THE_RINGS_3,
          Book.LORD_OF_THE_RINGS_3,
          Book.LORD_OF_THE_RINGS_3
        )

        When("computing final price")
        val finalPrice = Accounting.checkout(Accounting.buildBasket(books: _*))

        Then("final price is 99.00€") // (9.90 + 10.40 + (29.90 * 3)) * 0.90
        finalPrice shouldBe 99.00
      }
      "best promotion is OneBookFromAtLeastThreeDifferentCollection" in {
        Given("a list of various books")
        val books: List[Book] = List(
          Book.GAME_OF_THRONES_1,
          Book.GAME_OF_THRONES_2,
          Book.GAME_OF_THRONES_5,
          Book.GAME_OF_THRONES_5,
          Book.LORD_OF_THE_RINGS_1,
          Book.LORD_OF_THE_RINGS_1,
          Book.LORD_OF_THE_RINGS_2,
          Book.LORD_OF_THE_RINGS_3,
          Book.LORD_OF_THE_RINGS_3,
          Book.LORD_OF_THE_RINGS_3,
          Book.LORD_OF_THE_RINGS_3,
          Book.HARRY_POTTER_1,
          Book.HARRY_POTTER_1,
          Book.HARRY_POTTER_1,
          Book.HARRY_POTTER_1,
          Book.HARRY_POTTER_2,
          Book.HARRY_POTTER_2,
          Book.HARRY_POTTER_2,
          Book.HARRY_POTTER_3,
          Book.HARRY_POTTER_7,
          Book.HARRY_POTTER_7
        )

        When("computing final price")
        val finalPrice = Accounting.checkout(Accounting.buildBasket(books: _*))

        Then(
          "final price is 206.64€"
        ) // (9.90 + 10.40 + (11.90 * 2) + (19.90 * 2) + 24.90 + (29.90 * 4) + (9.90 * 4) + (10.90 * 3) + 11.90 + (15.90 * 2)) * 0.60
        finalPrice shouldBe 206.64
      }
    }
  }
}
