package fr.valwin.library

import fr.valwin.library.model.Promotion.{
  AtLeastThreeDifferentBooksFromTheSameCollection,
  OneBookFromAtLeastThreeDifferentCollection,
  OneBookFromTwoDifferentCollection,
  TwoDifferentBooksFromTheSameCollection
}
import fr.valwin.library.model.{
  Basket,
  BasketPromotion,
  Book,
  Collection,
  CollectionPromotion,
  Promotion
}

object Accounting {

  /** build a basket from books
    * @param books
    */
  def buildBasket(books: Book*): Basket = Basket(books.toList)

  /** compute the total price of the basket
    * @param basket
    */
  def checkout(basket: Basket): BigDecimal = {
    val booksByCollection: Map[Collection, List[Book]] = basket.books.groupBy(_.collection)

    val maybeBasketPromotion: Option[BasketPromotion] = booksByCollection.size match {
      case 0 | 1 => None
      case 2     => Some(OneBookFromTwoDifferentCollection(basket))
      case _     => Some(OneBookFromAtLeastThreeDifferentCollection(basket))
    }

    val collectionPromotions: List[CollectionPromotion] =
      booksByCollection.flatMap { case (collection, books) =>
        books.distinctBy(_.tome).length match {
          case 0 | 1 => None
          case 2 =>
            Some(TwoDifferentBooksFromTheSameCollection(basket, collection))
          case _ =>
            Some(
              AtLeastThreeDifferentBooksFromTheSameCollection(
                basket,
                collection
              )
            )
        }
      }.toList

    val bestReduction: Option[Promotion] =
      (maybeBasketPromotion.toList ++ collectionPromotions)
        .sortBy(_.getReduction)
        .lastOption

    (basket.getTotal - bestReduction.map(_.getReduction).sum)
      .setScale(2, BigDecimal.RoundingMode.HALF_UP)
  }
}
