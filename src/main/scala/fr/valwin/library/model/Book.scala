package fr.valwin.library.model

final case class Book(collection: Collection, tome: Int, price: Double)

object Book {
  /*
  Pas d'enum contrairement aux promotions car représentation d'une base de données
  Les promotions étant quant à elles orientées métier
   */
  val HARRY_POTTER_1: Book = Book(Collection.HarryPotter, 1, 9.90)
  val HARRY_POTTER_2: Book = Book(Collection.HarryPotter, 2, 10.90)
  val HARRY_POTTER_3: Book = Book(Collection.HarryPotter, 3, 11.90)
  val HARRY_POTTER_4: Book = Book(Collection.HarryPotter, 4, 12.90)
  val HARRY_POTTER_5: Book = Book(Collection.HarryPotter, 5, 13.90)
  val HARRY_POTTER_6: Book = Book(Collection.HarryPotter, 6, 14.90)
  val HARRY_POTTER_7: Book = Book(Collection.HarryPotter, 7, 15.90)
  val LORD_OF_THE_RINGS_1: Book = Book(Collection.LordOfTheRings, 1, 19.90)
  val LORD_OF_THE_RINGS_2: Book = Book(Collection.LordOfTheRings, 2, 24.90)
  val LORD_OF_THE_RINGS_3: Book = Book(Collection.LordOfTheRings, 3, 29.90)
  val GAME_OF_THRONES_1: Book = Book(Collection.GameOfThrones, 1, 9.90)
  val GAME_OF_THRONES_2: Book = Book(Collection.GameOfThrones, 2, 10.40)
  val GAME_OF_THRONES_3: Book = Book(Collection.GameOfThrones, 3, 10.90)
  val GAME_OF_THRONES_4: Book = Book(Collection.GameOfThrones, 4, 11.40)
  val GAME_OF_THRONES_5: Book = Book(Collection.GameOfThrones, 5, 11.90)
}
