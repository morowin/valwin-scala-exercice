package fr.valwin.library.model

import enumeratum.{Enum, EnumEntry}

sealed abstract class Collection(tomes: Int) extends EnumEntry

object Collection extends Enum[Collection] {
  case object HarryPotter extends Collection(7)
  case object LordOfTheRings extends Collection(3)
  case object GameOfThrones extends Collection(5)
  override def values: IndexedSeq[Collection] = findValues
}