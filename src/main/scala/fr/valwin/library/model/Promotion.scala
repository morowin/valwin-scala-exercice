package fr.valwin.library.model

import enumeratum.{Enum, EnumEntry}

sealed abstract class Promotion(percent: BigDecimal) extends EnumEntry {
  def getReduction: BigDecimal
}

sealed abstract class CollectionPromotion(
    basket: Basket,
    collection: Collection,
    percent: BigDecimal
) extends Promotion(percent) {
  override def getReduction: BigDecimal =
    basket.getCollectionTotal(collection) * (percent / 100)
}
sealed abstract class BasketPromotion(basket: Basket, percent: BigDecimal)
    extends Promotion(percent) {
  override def getReduction: BigDecimal = basket.getTotal * (percent / 100)
}

object Promotion extends Enum[Promotion] {
  case class TwoDifferentBooksFromTheSameCollection(
      basket: Basket,
      collection: Collection
  ) extends CollectionPromotion(basket, collection, 20)
  case class AtLeastThreeDifferentBooksFromTheSameCollection(
      basket: Basket,
      collection: Collection
  ) extends CollectionPromotion(basket, collection, 30)
  case class OneBookFromTwoDifferentCollection(basket: Basket)
      extends BasketPromotion(basket, 10)
  case class OneBookFromAtLeastThreeDifferentCollection(basket: Basket)
      extends BasketPromotion(basket, 40)
  override def values: IndexedSeq[Promotion] = findValues
}
