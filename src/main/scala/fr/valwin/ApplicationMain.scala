package fr.valwin

import java.time.Duration
import java.time.temporal.ChronoUnit

import fr.valwin.library.Accounting
import fr.valwin.library.model.{Basket, Book}

object ApplicationMain extends App {
  val start = System.currentTimeMillis()
  val basket = Basket.awesome
  val total = Accounting.checkout(basket)
  val duration =
    Duration
      .of(System.currentTimeMillis() - start, ChronoUnit.MILLIS)
  println(s"PRIX A PAYER : $total")
  println(s"TEMPS DE TRAITEMENT : $duration")
}
