package fr.valwin

object Exercises {

  /** 1. Appliquer la transformation suivante pour chaque élément de la liste list1 : aaaa => AAAA
    * Quel méthode de List[A] permet d'appliquer une lambda sur chaque élément ?
    */
  val list1: List[String] = List("Toto", "titi", "CACA")
  val res1: List[String] = list1.map(_.toUpperCase)

  /** 2. Sur les options suivantes (opt1 et opt2), récupérer pour chacune leur contenu, sinon retourner : 42
    * Quel méthode de Option[A] permet de récupérer le contenu de l'option ?
    * Quel méthode de Option[A] permet de récupérer le contenu de l'option s'il existe et d'en définir une par défaut dans le cas contraire ?
    */
  val opt1: Option[Int] = Some(2020)
  val opt2: Option[Int] = None
  val res2: Int = opt1.getOrElse(42)
  val res3: Int = opt2.getOrElse(42)

  /** 3. A partir de list2, ne garder que le contenu des Option si elles sont définies. Exemple : List(Some("a"), None)) => List("a")
    * Quel méthode de List[Option[A]] permet de flatter ?
    */
  val list2: List[Option[String]] =
    List(Some("Toto"), Some("titi"), None, None, Some("CACA"))
  val res4: List[String] = list2.flatten

  /** 4. A partir de list1, appliquer la fonction divideLengthByOccurrences pour chaque élément de la liste permettant de diviser
    *     la taille d'une chaîne de caractères par le nombre d'occurrences, dans cette chaîne, du caractère 't'
    * Quel méthode de List[A] permet de transformer vers List[Option[B]] tout en flattant pour obtenir List[B]
    */
  def divideLengthByOccurrences(str: String, c: Char): Option[Double] = {
    val occurrences = str.filter(_ == c).length
    Option.when(occurrences != 0)(str.length / occurrences)
  }
  val res5: List[Double] = list1.flatMap(divideLengthByOccurrences(_, 't'))
}
